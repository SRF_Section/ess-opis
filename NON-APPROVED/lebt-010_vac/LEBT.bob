<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>LEBT Vacuum System</name>
  <macros>
    <DIS>Vac</DIS>
    <ROOT>$(ESS_OPIS=/ess-opis)/NON-APPROVED</ROOT>
    <SEC>LEBT</SEC>
    <SUBSEC>010</SUBSEC>
    <WIDGET_ROOT>$(ROOT)/COMMON/DEVICES/vacuum</WIDGET_ROOT>
    <vacSYMBOLS>$(ROOT)/COMMON/DEVICES/vacuum/symbols</vacSYMBOLS>
  </macros>
  <width>2200</width>
  <height>1200</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <text>            LEBT Vacuum System</text>
    <width>2200</width>
    <height>80</height>
    <font>
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color>
      <color name="White" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="Primary Blue" red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open VGC Trends Action</name>
    <actions>
      <action type="open_display">
        <file>Trending LEBT-010 VGC.bob</file>
        <target>tab</target>
        <description>Open VGC Trends</description>
      </action>
    </actions>
    <text>Open
VGC Trends</text>
    <x>1300</x>
    <y>10</y>
    <width>150</width>
    <height>60</height>
    <font>
      <font name="GROUP-HEADER" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK-TEXT" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Diagnostics Action</name>
    <actions>
      <action type="open_display">
        <file>diagnostics.bob</file>
        <target>window</target>
        <description>Open Diagnostics</description>
      </action>
    </actions>
    <text>Open
Diagnostics</text>
    <x>1500</x>
    <y>10</y>
    <width>150</width>
    <height>60</height>
    <font>
      <font name="GROUP-HEADER" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK-TEXT" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Legend Action</name>
    <actions>
      <action type="open_display">
        <file>$(WIDGET_ROOT)/COMMON/legend.bob</file>
        <target>window</target>
        <description>Open Legend</description>
      </action>
    </actions>
    <text>Open
Legend</text>
    <x>1700</x>
    <y>10</y>
    <width>150</width>
    <height>60</height>
    <font>
      <font name="GROUP-HEADER" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="BLACK-TEXT" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <transparent>true</transparent>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ISRC-010</name>
    <text>ISRC-010</text>
    <x>75</x>
    <y>150</y>
    <width>150</width>
    <height>50</height>
    <font>
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Hydrogen bottle</name>
    <macros>
      <Gas>H2</Gas>
    </macros>
    <file>$(WIDGET_ROOT)/gas-cylinder/gas-cylinder.bob</file>
    <x>63</x>
    <y>555</y>
    <width>38</width>
    <height>86</height>
    <resize>1</resize>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>ISRC Separator</name>
    <y>102</y>
    <width>300</width>
    <height>539</height>
    <points>
      <point x="300.0" y="0.0">
      </point>
      <point x="300.0" y="539.0">
      </point>
      <point x="0.0" y="539.0">
      </point>
    </points>
    <line_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </line_color>
    <line_style>1</line_style>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>From Hydrogen</name>
    <x>82</x>
    <y>495</y>
    <width>24</width>
    <height>64</height>
    <points>
      <point x="0.0" y="64.0">
      </point>
      <point x="0.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="group" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX) Group</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>01100</IDX>
      <SEC>ISrc</SEC>
    </macros>
    <x>11</x>
    <y>439</y>
    <width>135</width>
    <height>72</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Device</name>
      <text>$(DEV)</text>
      <width>40</width>
      <font>
        <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Index</name>
      <text>$(IDX)</text>
      <y>30</y>
      <width>40</width>
      <font>
        <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>PIPE_RIGHT</name>
      <x>96</x>
      <width>37</width>
      <height>50</height>
      <points>
        <point x="0.0" y="25.0">
        </point>
        <point x="37.0" y="25.0">
        </point>
      </points>
    </widget>
    <widget type="polyline" version="2.0.0">
      <name>PIPE_BELOW</name>
      <x>46</x>
      <y>50</y>
      <width>50</width>
      <points>
        <point x="25.0" y="0.0">
        </point>
        <point x="25.0" y="20.0">
        </point>
      </points>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>Symbol</name>
      <pv_name>loc://$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX):UI:StatusR&lt;VEnum&gt;(0, "Invalid", "Opened", "Closed")</pv_name>
      <symbols>
        <symbol>$(vacSYMBOLS)/vva/vva-invalid.png</symbol>
        <symbol>$(vacSYMBOLS)/vva/vva-open.png</symbol>
        <symbol>$(vacSYMBOLS)/vva/vva-closed.png</symbol>
      </symbols>
      <x>46</x>
      <width>50</width>
      <height>50</height>
      <background_color>
        <color name="WHITE" red="255" green="255" blue="255">
        </color>
      </background_color>
      <rules>
        <rule name="Tooltip" prop_id="tooltip" out_exp="true">
          <exp bool_exp="true">
            <expression>pvStr0</expression>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
      <scripts>
        <script file="EmbeddedJs" check_connections="false">
          <text><![CDATA[PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var pvOpen      = 0;
var pvSymbol    = pvs[0];

var colorID = 0;

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
        debug = debug[0];
        switch (debug) {
                case '1':
                case 'Y':
                case 'y':
                case 'T':
                case 't':
                        debug = true;
                        break;

                default:
                        debug = false;
        }
}
else
        debug = false;

if (debug)
        Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
        Logger = new Object();
        Logger.info = function() {}
        Logger.warning = function() {}
        Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
        Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
        pvOpen = PVUtil.getInt(pvs[1]);

        log_pv(pvs[1]);

        if (pvOpen) {
                Logger.info(pvSymbol + ": OPEN");
                colorID = 1;
        } else {
                Logger.info(pvSymbol + ": CLOSED");
                colorID = 2;
        }
} catch (err) {
        Logger.severe("NO CONNECTION: " + err);
}

pvSymbol.write(colorID);
]]></text>
          <pv_name trigger="false">$(pv_name)</pv_name>
          <pv_name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX):OpenR</pv_name>
        </script>
      </scripts>
      <tooltip>$(pv_value)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Open Faceplate</name>
      <actions>
        <action type="open_display">
          <file>$(WIDGET_ROOT)/vva/Faceplate/vac_isrc-vva.bob</file>
          <target>standalone</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text></text>
      <width>46</width>
      <height>50</height>
      <transparent>true</transparent>
      <tooltip>Open faceplate</tooltip>
    </widget>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVMC-$(IDX)</name>
    <macros>
      <CONTROLLER>ISrc-$(SUBSEC):$(DIS)-VEVMC-01100</CONTROLLER>
      <DEV>VVMC</DEV>
      <IDX>01100</IDX>
      <SEC>ISrc</SEC>
    </macros>
    <file>$(WIDGET_ROOT)/vvmc/vac_vvmc.bob</file>
    <x>144</x>
    <y>424</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Source</name>
    <text>Source</text>
    <x>149</x>
    <y>498</y>
    <width>44</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Arrow Source</name>
    <x>198</x>
    <y>507</y>
    <width>50</width>
    <height>1</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="50.0" y="0.0">
      </point>
    </points>
    <arrows>2</arrows>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>ISrc-$(SUBSEC):$(DIS)-VEVMC-01100</CONTROLLER>
      <DEV>VGD</DEV>
      <IDX>01100</IDX>
      <PIPE_RIGHT>true</PIPE_RIGHT>
      <SEC>ISrc</SEC>
    </macros>
    <file>$(WIDGET_ROOT)/vgd/vac_vgd.bob</file>
    <x>144</x>
    <y>532</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>From ISrc VVMC-01100</name>
    <x>276</x>
    <y>463</y>
    <width>2</width>
    <height>95</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="95.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe Source</name>
    <x>249</x>
    <y>507</y>
    <width>1750</width>
    <height>1</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="1750.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LEBT-010</name>
    <text>LEBT-010</text>
    <x>1216</x>
    <y>150</y>
    <width>150</width>
    <height>50</height>
    <font>
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color>
      <color name="Grid" red="169" green="169" blue="169">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 10000</name>
    <x>460</x>
    <y>297</y>
    <width>1</width>
    <height>211</height>
    <points>
      <point x="0.0" y="210.0">
      </point>
      <point x="0.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>From VVM-01100</name>
    <x>1060</x>
    <y>188</y>
    <width>3</width>
    <height>319</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="319.0">
      </point>
    </points>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Nitrogen bottle</name>
    <macros>
      <Gas>N2</Gas>
    </macros>
    <file>$(WIDGET_ROOT)/gas-cylinder/gas-cylinder.bob</file>
    <x>775</x>
    <y>406</y>
    <width>38</width>
    <height>86</height>
    <resize>1</resize>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>From Nitrogen</name>
    <x>794</x>
    <y>185</y>
    <width>25</width>
    <height>225</height>
    <points>
      <point x="0.0" y="225.00000000000003">
      </point>
      <point x="0.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 00031</name>
    <x>500</x>
    <y>790</y>
    <width>117</width>
    <height>110</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="61.0">
      </point>
      <point x="117.0" y="61.0">
      </point>
      <point x="117.0" y="110.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 02100</name>
    <x>405</x>
    <y>507</y>
    <width>99</width>
    <height>282</height>
    <points>
      <point x="99.0" y="0.0">
      </point>
      <point x="99.0" y="130.0">
      </point>
      <point x="0.0" y="130.0">
      </point>
      <point x="0.0" y="282.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 03100</name>
    <x>650</x>
    <y>507</y>
    <width>110</width>
    <height>282</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="130.0">
      </point>
      <point x="110.0" y="130.0">
      </point>
      <point x="110.0" y="282.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 00021</name>
    <x>317</x>
    <y>760</y>
    <width>1</width>
    <height>29</height>
    <points>
      <point x="0.0" y="28.0">
      </point>
      <point x="0.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Second line</name>
    <x>273</x>
    <y>789</y>
    <width>1760</width>
    <height>1</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="1760.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 07100</name>
    <x>1698</x>
    <y>507</y>
    <width>135</width>
    <height>282</height>
    <points>
      <point x="0.0" y="0.0">
      </point>
      <point x="0.0" y="126.0">
      </point>
      <point x="135.0" y="126.0">
      </point>
      <point x="135.0" y="282.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 30000</name>
    <x>1754</x>
    <y>297</y>
    <width>1</width>
    <height>211</height>
    <points>
      <point x="0.0" y="210.0">
      </point>
      <point x="0.0" y="0.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 00071</name>
    <x>1536</x>
    <y>790</y>
    <width>120</width>
    <height>110</height>
    <points>
      <point x="120.0" y="0.0">
      </point>
      <point x="120.0" y="61.0">
      </point>
      <point x="0.0" y="61.0">
      </point>
      <point x="0.0" y="110.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>Pipe 06100</name>
    <x>1401</x>
    <y>507</y>
    <width>135</width>
    <height>282</height>
    <points>
      <point x="135.0" y="0.0">
      </point>
      <point x="135.0" y="126.0">
      </point>
      <point x="0.0" y="126.0">
      </point>
      <point x="0.0" y="282.0">
      </point>
    </points>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>00021</IDX>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
    <x>140</x>
    <y>764</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>VVM-00021 Leak Test</name>
    <text>Leak Test</text>
    <x>179</x>
    <y>835</y>
    <width>65</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-00011</CONTROLLER>
      <DEV>VGP</DEV>
      <IDX>00021</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <RELAY1_DESC>Process PLC: Manifold under vacuum</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: (VVA-02100 + VVA-03100) Local protection</RELAY2_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgp/vac_vgp.bob</file>
    <x>246</x>
    <y>690</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>02100</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>334</x>
    <y>612</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VPT-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPT-$(IDX)</CONTROLLER>
      <DEV>VPT</DEV>
      <IDX>02100</IDX>
      <PIPE_ABOVE>true</PIPE_ABOVE>
      <PIPE_BELOW>true</PIPE_BELOW>
    </macros>
    <file>$(WIDGET_ROOT)/vpt/vac_vpt.bob</file>
    <x>433</x>
    <y>535</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VPT-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPT-$(IDX)</CONTROLLER>
      <DEV>VPT</DEV>
      <IDX>03100</IDX>
      <PIPE_ABOVE>true</PIPE_ABOVE>
      <PIPE_BELOW>true</PIPE_BELOW>
    </macros>
    <file>$(WIDGET_ROOT)/vpt/vac_vpt.bob</file>
    <x>579</x>
    <y>535</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>03100</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_LEFT>true</PIPE_LEFT>
      <ROTATION>90</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>689</x>
    <y>612</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <BYPASS>true</BYPASS>
      <DEV>VVA</DEV>
      <IDX>00041</IDX>
      <PIPE_HORIZONTAL>true</PIPE_HORIZONTAL>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_straight-vva.bob</file>
    <x>912</x>
    <y>764</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>01100</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>723</x>
    <y>118</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>00031</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_LEFT>true</PIPE_LEFT>
      <ROTATION>90</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>546</x>
    <y>826</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VPDP-$(IDX)</name>
    <macros>
      <DEV>VPDP</DEV>
      <IDX>00031</IDX>
      <PIPE_ABOVE>true</PIPE_ABOVE>
    </macros>
    <file>$(WIDGET_ROOT)/vpp-vpdp/vac_vpp-vpdp.bob</file>
    <x>546</x>
    <y>900</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVS-$(IDX)</name>
    <macros>
      <DEV>VVS</DEV>
      <IDX>20000</IDX>
      <PIPE_HORIZONTAL>true</PIPE_HORIZONTAL>
    </macros>
    <file>$(WIDGET_ROOT)/vvs/vac_vvs.bob</file>
    <x>1161</x>
    <y>482</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
      <DEV>VGC</DEV>
      <IDX>10000</IDX>
      <PIPE_LEFT>true</PIPE_LEFT>
      <RELAY1_DESC>Interlock PLC: Gates Valves Interlock (MPS)</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: High vacuum thresold , starting finished. (transition from starting to started)</RELAY2_DESC>
      <RELAY3_DESC>Process PLC: Gas Injection VVA Interlock</RELAY3_DESC>
      <RELAY4_DESC>&lt;not wired&gt;</RELAY4_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgc/vac_vgc.bob</file>
    <x>462</x>
    <y>329</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
      <DEV>VGP</DEV>
      <IDX>10000</IDX>
      <PIPE_RIGHT>true</PIPE_RIGHT>
      <RELAY1_DESC>Process PLC: Atmospheric pressure</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: "Vacuum" -&gt; Threshold to start the VPTs</RELAY2_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgp/vac_vgp.bob</file>
    <x>325</x>
    <y>329</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VGR</DEV>
      <IDX>10000</IDX>
      <PIPE_LEFT>true</PIPE_LEFT>
    </macros>
    <file>$(WIDGET_ROOT)/vgr/vac_vgr.bob</file>
    <x>462</x>
    <y>402</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEVMC-10001</CONTROLLER>
      <DEV>VGD</DEV>
      <IDX>10000</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <RELAY1_DESC>Interlock PLC: Not used</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: Not used</RELAY2_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgd/vac_vgd.bob</file>
    <x>389</x>
    <y>257</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVMC-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEVMC-10001</CONTROLLER>
      <DEV>VVMC</DEV>
      <IDX>01100</IDX>
    </macros>
    <file>$(WIDGET_ROOT)/vvmc/vac_vvmc.bob</file>
    <x>856</x>
    <y>103</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>00012</IDX>
      <PIPE_VERTICAL>true</PIPE_VERTICAL>
      <ROTATION>270</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_straight-vvm.bob</file>
    <x>723</x>
    <y>189</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>VVM-00012 Regulator</name>
    <text>Regulator</text>
    <x>819</x>
    <y>206</y>
    <width>65</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>00011</IDX>
      <PIPE_VERTICAL>true</PIPE_VERTICAL>
      <ROTATION>270</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_straight-vvm.bob</file>
    <x>723</x>
    <y>331</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>VVM-00011 Cylinder Valve</name>
    <text>Cylinder Valve</text>
    <x>819</x>
    <y>348</y>
    <width>101</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>01100</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_LEFT>true</PIPE_LEFT>
      <ROTATION>90</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
    <x>989</x>
    <y>118</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>10000</IDX>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
    <x>325</x>
    <y>402</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>VVM-10000 Venting</name>
    <text>Venting</text>
    <x>371</x>
    <y>474</y>
    <width>51</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>00071</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>1465</x>
    <y>826</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>06100</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>1330</x>
    <y>608</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VVA</DEV>
      <IDX>07100</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <PIPE_LEFT>true</PIPE_LEFT>
      <ROTATION>90</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vva/vac_angle-vva.bob</file>
    <x>1762</x>
    <y>608</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VPDP-$(IDX)</name>
    <macros>
      <DEV>VPDP</DEV>
      <IDX>00071</IDX>
      <PIPE_ABOVE>true</PIPE_ABOVE>
    </macros>
    <file>$(WIDGET_ROOT)/vpp-vpdp/vac_vpp-vpdp.bob</file>
    <x>1465</x>
    <y>900</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VPT-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPT-$(IDX)</CONTROLLER>
      <DEV>VPT</DEV>
      <IDX>06100</IDX>
      <PIPE_ABOVE>true</PIPE_ABOVE>
      <PIPE_BELOW>true</PIPE_BELOW>
    </macros>
    <file>$(WIDGET_ROOT)/vpt/vac_vpt.bob</file>
    <x>1465</x>
    <y>535</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VPT-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEPT-$(IDX)</CONTROLLER>
      <DEV>VPT</DEV>
      <IDX>07100</IDX>
      <PIPE_ABOVE>true</PIPE_ABOVE>
      <PIPE_BELOW>true</PIPE_BELOW>
    </macros>
    <file>$(WIDGET_ROOT)/vpt/vac_vpt.bob</file>
    <x>1627</x>
    <y>535</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
      <DEV>VGC</DEV>
      <IDX>30000</IDX>
      <PIPE_LEFT>true</PIPE_LEFT>
      <RELAY1_DESC>Interlock PLC: Gates Valves Interlock (MPS)</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: High vacuum thresold , starting finished. (transition from starting to started)</RELAY2_DESC>
      <RELAY3_DESC>Process PLC: Not used</RELAY3_DESC>
      <RELAY4_DESC>&lt;not wired&gt;</RELAY4_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgc/vac_vgc.bob</file>
    <x>1756</x>
    <y>329</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-10001</CONTROLLER>
      <DEV>VGP</DEV>
      <IDX>30000</IDX>
      <PIPE_RIGHT>true</PIPE_RIGHT>
      <RELAY1_DESC>Process PLC: Atmospheric pressure</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: "Vacuum" -&gt; Threshold to start the VPTs</RELAY2_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgp/vac_vgp.bob</file>
    <x>1619</x>
    <y>329</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>30000</IDX>
      <PIPE_RIGHT>true</PIPE_RIGHT>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
    <x>1619</x>
    <y>402</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>VVM-30000 Venting</name>
    <text>Venting</text>
    <x>1664</x>
    <y>474</y>
    <width>51</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVS-$(IDX)</name>
    <macros>
      <DEV>VVS</DEV>
      <IDX>40000</IDX>
    </macros>
    <file>$(WIDGET_ROOT)/vvs/vac_vvs.bob</file>
    <x>1992</x>
    <y>482</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <CONTROLLER>$(SEC)-$(SUBSEC):$(DIS)-VEG-00011</CONTROLLER>
      <DEV>VGP</DEV>
      <IDX>00081</IDX>
      <PIPE_BELOW>true</PIPE_BELOW>
      <RELAY1_DESC>Process PLC: Manifold under vacuum</RELAY1_DESC>
      <RELAY2_DESC>Process PLC: (VVA-06100 + VVA-07100) Local protection</RELAY2_DESC>
    </macros>
    <file>$(WIDGET_ROOT)/vgp/vac_vgp.bob</file>
    <x>1896</x>
    <y>717</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-VVM-$(IDX)</name>
    <macros>
      <DEV>VVM</DEV>
      <IDX>00081</IDX>
      <PIPE_LEFT>true</PIPE_LEFT>
      <ROTATION>90</ROTATION>
    </macros>
    <file>$(WIDGET_ROOT)/vvm/vac_angle-vvm.bob</file>
    <x>2031</x>
    <y>764</y>
    <width>133</width>
    <height>70</height>
    <resize>2</resize>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>VVM-00081 Venting</name>
    <text>Venting</text>
    <x>2077</x>
    <y>835</y>
    <width>51</width>
    <height>16</height>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VPG</DEV>
      <IDX>10001</IDX>
    </macros>
    <file>$(WIDGET_ROOT)/vpg/vac-vpg.bob</file>
    <x>720</x>
    <y>850</y>
    <width>320</width>
    <height>304</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</name>
    <macros>
      <DEV>VPG</DEV>
      <IDX>20001</IDX>
    </macros>
    <file>$(WIDGET_ROOT)/vpg/vac-vpg.bob</file>
    <x>1682</x>
    <y>850</y>
    <width>320</width>
    <height>304</height>
    <resize>2</resize>
  </widget>
</display>
